#!/vsharma/bin/env python3

# smtplib module send mail

import smtplib

TO = 's.vyneet@gmail.com'
SUBJECT = 'TEST MAIL'
TEXT = 'Here is a message from python.'

# Gmail Sign In
gmail_sender = 's.vyneet@gmail.com'


server = smtplib.SMTP('gmail-smtp-in.l.google.com:25')
server.ehlo()
server.starttls()

BODY = '\r\n'.join(['To: %s' % TO,
                    'From: %s' % gmail_sender,
                    'Subject: %s' % SUBJECT,
                    '', TEXT])

try:
    server.sendmail(gmail_sender, [TO], BODY)
    print ('email sent')
except:
    print ('error sending mail')

server.quit()
