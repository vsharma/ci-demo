#!/bin/sh

## The ruby that comes with Debian Jessie is too old, so let's switch:
source /etc/profile.d/rvm.sh
rvm use ruby-2.2

find -type f -iname "*.csv" | \
while read csv_file_name
do
	if ! csvlint "$csv_file_name"
	then
		echo "Warnung: $csv_file_name hat die Prüfung durch csvlint nicht bestanden."
		exit 1
	fi
done
